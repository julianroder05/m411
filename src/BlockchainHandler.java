import java.util.ArrayList;

/**
 * Provides means to add, get and dump the blockchain.
 * @author Julian Roder
 */
public class BlockchainHandler {

    public ArrayList<Block> blockchain = new ArrayList<Block>();

    public void add(Image data, String previousHash) {
        blockchain.add(new Block(data, previousHash));
    }

    public Block get(String hash) {
        Block block = null;
        for (int i = 0; i < blockchain.size(); i++) {
            if (blockchain.get(i).hash.equals(hash)) {
                block = blockchain.get(i);
            }
        }

        return block;
    }
    
    public void dump() {
        System.out.println("\nDie Blockchain enthält foldene Bilder:\n");
        for (int i = 0; i < blockchain.size(); i++) {
            Image image = blockchain.get(i).getData();
            System.out.println("Id: " + image.getId());
            System.out.println("Name: " + image.name);
            System.out.println("Path: " + image.path);
            System.out.println();
        }
    }
}
