/**
 * Represents an Image with an id, name, path and bytes.
 * @author Julian Roder
 */
public class Image {
    private int id;
    public String name;
    public String path;
    public byte[] bytes;

    public Image(String name, String path, byte[] bytes) {
        this.name = name;
        this.path = path;
        this.bytes = bytes;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
