import java.util.Date;

/**
 * Represents a block in a blockchain.
 * @author Julian Roder
 */
public class Block {
    public String hash;
    public String previousHash;
    private Image data;
    private long timestamp;

    public Block(Image data, String previousHash) {
        this.data = data;
        this.previousHash = previousHash;
        this.timestamp = new Date().getTime();
        this.hash = calculateHash();
    }

    public String calculateHash() {
        String calculatedHash = Crypt.sha256(previousHash + Long.toString(timestamp) + data.name);

        return calculatedHash;
    }

    public Image getData() {
        return data;
    }

    public void setData(Image data) {
        this.data = data;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }
}
