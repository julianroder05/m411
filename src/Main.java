import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    private static BlockchainHandler blockchainHandler = new BlockchainHandler();

    public static void main(String[] args) {
        int numberOfImages = numberOfImages();
        ArrayList<Image> images = userInteraction(numberOfImages);

        addImagesToBlockchain(images);

        if (wantsToOpenImage()) {
            openImagesDialog();
        }

        if (wantsToDeleteImages()) {
            deleteImages();
        }
    }

    private static ArrayList<Image> userInteraction(int numberOfImages) {
        ArrayList<Image> images = new ArrayList<>();
        Scanner scanner = new Scanner(System.in);

        for (int i = 0; i < numberOfImages; i++) {
            System.out.println("Geben sie die URL zum " + (i + 1) + "ten bild ein:");
            String url = scanner.nextLine();
            Image image = fetchFromWeb(url);
            image.setId(i + 1);
            images.add(image);
        }

        return images;
    }

    private static Image fetchFromWeb(String url) {
        Image image = null;
        try {
            image = ImageHandler.saveOnDisk(url);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return image;
    }

    private static void addImagesToBlockchain(ArrayList<Image> images) {
        for (int i = 0; i < images.size(); i++) {
            if (i == 0) {
                blockchainHandler.add(images.get(i), "0");
            } else {
                blockchainHandler.add(images.get(i), blockchainHandler.blockchain.get(blockchainHandler.blockchain.size() - 1).hash);
            }
        }
    }

    private static int numberOfImages() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Wie viele Bilder wollen Sie in der Blockchain speichern:");
        int numberOfImages = scanner.nextInt();

        return numberOfImages;
    }

    private static void openImagesDialog() {
        Scanner scanner = new Scanner(System.in);
        boolean stop = false;
        do {
            blockchainHandler.dump();
            System.out.println("\nWelches der aufgelisteten Bilder wollen Sie öffnen (id eingeben):");
            int imageId = scanner.nextInt();
            openImage(blockchainHandler.blockchain.get(imageId - 1).getData().path);
            stop = wantsToStop();
        } while (!stop);
    }

    private static boolean wantsToStop() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Wollen Sie ein weiteres Bild öffnen(y/n)");
        String input = scanner.nextLine();
        char test = input.charAt(0);
        return test != 'y';
    }

    private static boolean wantsToOpenImage() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Möchten Sie eines der Bilder öffnen(y/n)");
        String input = scanner.nextLine();

        return input.charAt(0) == 'y';
    }

    private static void openImage(String path) {
        File file = new File(path);
        Desktop dt = Desktop.getDesktop();
        try {
            dt.open(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static boolean wantsToDeleteImages() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Möchten Sie die Bilder wieder löschen(y/n)");
        String input = scanner.nextLine();

        return input.charAt(0) == 'y';
    }

    private static void deleteImages() {
        for (int i = 0; i < blockchainHandler.blockchain.size(); i++) {
            File f = new File(blockchainHandler.blockchain.get(i).getData().path);
            f.delete();
        }
        System.out.println("Bilder wurden vom System gelöscht");
    }
}
