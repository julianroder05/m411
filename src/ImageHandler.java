import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Provides means to get an image from https and save it on the local disk.
 * @author Julian Roder
 */
public class ImageHandler {

    private static byte[] getFromWeb(URL url) {
        ByteArrayOutputStream output = new ByteArrayOutputStream();

        try (InputStream inputStream = url.openStream()) {
            int n = 0;
            byte [] buffer = new byte[ 1024 ];
            while (-1 != (n = inputStream.read(buffer))) {
                output.write(buffer, 0, n);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return output.toByteArray();
    }

    public static Image saveOnDisk(String url) throws IOException {
        byte[] bytes = getFromWeb(new URL(url));
        String name = url.substring(url.lastIndexOf('/')+1, url.length());
        String path = System.getProperty("java.io.tmpdir") + name;
        OutputStream out = new FileOutputStream(path);
        out.write(bytes);
        out.flush();
        out.close();

        return new Image(name, path, bytes);
    }

    public static byte[] getBytesFromImage(String url) throws MalformedURLException {
        return getFromWeb(new URL(url));
    }
}
